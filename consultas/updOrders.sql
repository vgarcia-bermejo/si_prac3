﻿
--DROP TRIGGER if exists updOrders1 on orderdetail;
--DROP TRIGGER if exists updOrders2 on orderdetail;
--DROP TRIGGER if exists updOrders3 on orderdetail;
DROP FUNCTION if exists triggerOrders() CASCADE;

CREATE OR REPLACE FUNCTION triggerOrders() RETURNS TRIGGER AS $$
	DECLARE

		BEGIN
	-- Juntamos en una vista el orderid, el precio del order y todas las columnas de orders

	Create view vista as

		SELECT orders.orderid, SUM (price*quantity) FROM orderdetail
			join orders on orders.orderid = orderdetail.orderid group by orders.orderid;

	--actualizamos el precio del order en netamount

	UPDATE orders set netamount = vista.sum from vista where orders.orderid = vista.orderid;

	-- con el netamount calculado calculamos el totalamount, que es el precio total del pedido (precio del pedido + tasas)

	UPDATE orders SET totalamount = orders.netamount + orders.tax;

	drop view vista;
	return NEW;

	END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER updOrders2 after delete on orderdetail
FOR EACH ROW EXECUTE PROCEDURE triggerOrders();
CREATE TRIGGER updOrders3 after update on orderdetail
FOR EACH ROW EXECUTE PROCEDURE triggerOrders();
