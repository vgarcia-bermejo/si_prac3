
-- La funcion gettopventas devuelve las películas que más se han vendido en un anyo junto con el numero de ventas en ese anyo de la
--pelicula.
-- Para ello hemos creado una tabla que contendra el resultado de la funcion (los topventas por anyo) que la dropeamos si existe antes
-- para evitar errores por si ya está creada.
-- Hacemos la consulta que nos saque la pelicula más vendida junto con el numero de ventas de esa pelicula ese año y la insertamos
-- en la tabla creada anteriormente, y esta consulta la ejecutamos en un for que recorre los diferentes anyos en los que se han
-- comprado las peliculas
-- Por ultimo devolvemos la query que selecciona todo lo que contiene la tabla
CREATE OR REPLACE FUNCTION gettopventas(year TEXT) RETURNS TABLE (ano TEXT, titulo VARCHAR(225), ventas INTEGER) AS $$
	DECLARE
		years alias for $1;

	BEGIN
		DROP TABLE if exists masVendidos;
		CREATE TABLE masVendidos (
			ano text,
			titulo VARCHAR(225),
			ventas INTEGER
		);

		FOR years IN (select * from (select distinct(extract(year from orders.orderdate)) as auxyear from orders order by auxyear) as t1 where t1.auxyear>=year::numeric) LOOP
			insert into masVendidos select years,imdb_movies.movieTitle, sum(orderdetail.quantity) as ventas
			from orders,orderdetail, imdb_movies, products where extract(year from orders.orderdate)::text=years::text
			and orders.orderid=orderdetail.orderid and orderdetail.prod_id=products.prod_id and products.movieId = imdb_movies.movieid
			group by imdb_movies.movieid order by ventas desc limit 1;

		END LOOP;
	RETURN QUERY SELECT * FROM masVendidos order by ano desc;
END;
$$
LANGUAGE plpgsql;
