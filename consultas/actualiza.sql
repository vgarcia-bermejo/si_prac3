-- Le añadimos a la tabla customers un campo llamado cypherpassword que contiene la contrasenya cifrada con ayuda de otra tabla

alter table customers add column cypherpassword Text;

create table auxcustomers(
	id numeric,
	password VARCHAR(50)
);


insert into auxcustomers(id, password) select customerid,md5(password) from customers order by customerid;

update customers set cypherpassword = auxcustomers.password from auxcustomers where auxcustomers.id=customers.customerid;

drop table auxcustomers;

-- Cambiamos columnas de la tabla de customers para que a la hora de registro haya columnas que puedas estar vacias

ALTER TABLE customers ALTER COLUMN firstname DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN lastname DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN address1 DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN city DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN country DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN region DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN creditcardtype DROP NOT NULL;
ALTER TABLE customers ALTER COLUMN creditcardexpiration DROP NOT NULL;



-- Se añaden foreign keys (prod_id y orderid) a la tabla orderdetail

alter table orderdetail add foreign key (prod_id)
	references products(prod_id) on update cascade;
alter table orderdetail add foreign key (orderid)
	references orders(orderid) on update cascade;

-- Creamos la tabla alerts que se llenará cuando no haya stock de un producto

CREATE TABLE Alertas(
	prod_id integer NOT NULL,
	FOREIGN KEY (prod_id) REFERENCES Products(prod_id)
);

-- Se deshace el atributo multivaluado de imdb_movies, creando una tabla que contenga los diferentes géneros de forma única

 create table imdb_genres(
	genero VARCHAR(32) primary key
 );

insert into imdb_genres (genero) select distinct genre from imdb_moviegenres;

alter table imdb_moviegenres add foreign key (genre) references imdb_genres (genero) on update cascade;

-- Se deshace el atributo multivaluado de imdb_movies, creando una tabla que tenga los diferentes países de forma única

 create table imdb_countries(
	pais VARCHAR(32) primary key
 );

insert into imdb_countries (pais) select distinct country from imdb_moviecountries;

alter table imdb_moviecountries add foreign key (country) references imdb_countries (pais) on update cascade;

-- Se deshace el atributo multivaluado de imdb_movies, creando una tabla con los diferentes idiomas

 create table imdb_languages(
	idioma VARCHAR(32) primary key
 );

insert into imdb_languages (idioma) select distinct language from imdb_movielanguages;

alter table imdb_movielanguages add foreign key (language) references imdb_languages (idioma) on update cascade;

-- Depuramos la tabla orderid de forma que no tenga claves primarias (orderid, prod_id) repetidos, se vuelca en una tabla auxiliar
-- se vacía la tabla original y se vuelve a rellenar la original con los datos depurados de la auxiliar

create table auxodet (
	orderid integer NOT NULL,
  prod_id integer NOT NULL,
  price numeric, -- price without taxes when the order was paid
  quantity integer
  );

insert into auxodet (orderid, prod_id) select distinct (orderid), prod_id from orderdetail;

update auxodet set quantity = orderdetail.quantity from orderdetail where auxodet.orderid = orderdetail.orderid and auxodet.prod_id = orderdetail.prod_id;

truncate orderdetail;

insert into orderdetail (orderid, prod_id, price, quantity) select * from auxodet;

alter table orderdetail add primary key (orderid, prod_id);

drop  table auxodet;

-- Se añade como foreign key customerid en orders
alter table orders add foreign key (customerid) references customers (customerid);

-- actualizamos las dos peliculas que hay en la base de datos con un año del tipo 1998-1999
--little men
UPDATE imdb_movies SET year = 1998 WHERE movieid = 636347;
-- Magic Hour
UPDATE imdb_movies SET year = 1998 WHERE movieid = 644338;

-- Actualizamos los customers de forma que no haya usuarios con el username repetido, por lo que los repetidos modificamos su nombr
-- concatenandolos con su id


Create view aux as
	Select username, Count(username) From customers group by username having (count(username) > 1);

Update customers set username = username||customerid where customerid in (select customerid FROM customers where username in (select username from aux));

drop view aux;
