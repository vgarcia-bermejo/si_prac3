
-- Creamos una vista que calcule el precio antiguo así: precioactual*(1/1.02)^fechaActual-fechadelorder
create view setprice as
 SELECT orderdetail.orderid, (round(products.price * (1/((1.02) ^ (extract(year from (now())) - extract(year from(orders.orderdate))))::numeric),2)::numeric) as calculo
FROM products, orders, orderdetail WHERE products.prod_id = orderdetail.prod_id and orderdetail.orderid = orders.orderid;

--hacemos el update para actualizar precios este update tarda alrededor de 1 minuto en ejecutarse

update orderdetail set price = setprice.calculo from setprice, products where orderdetail.orderid = setprice.orderid and orderdetail.prod_id = products.prod_id; 

drop view setprice;