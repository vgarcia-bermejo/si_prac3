DROP FUNCTION if exists triggerUpdate();

CREATE OR REPLACE FUNCTION triggerUpdate() returns trigger as $$
	DECLARE
		ord int4;
		prod int4;
	BEGIN
		ord=old.orderid;
		FOR prod IN (select prod_id from orderdetail where orderid=ord) LOOP
			-- Si el producto ya esta sin stock metelo en alertas
			IF ((select stock from inventory where prod_id=prod) <= 0) THEN
				INSERT INTO alertas(prod_id) VALUES (prod);
			-- Si el la cantidad que se quiere comprar es mayor a la de stock del producto salta
			ELSIF (((select stock from inventory where prod_id=prod)-(select quantity from orderdetail where prod_id=prod and orderid=ord))< 0) THEN
				RAISE NOTICE '%; SQLSTATE: %', SQLERRM, SQLSTATE;
			-- Si la cantidad de productos que se desea comprar deja sin stock metelo a alertas y realiza la compra
			ELSIF (((select stock from inventory where prod_id=prod)-(select quantity from orderdetail where prod_id=prod and orderid=ord))= 0) THEN
				INSERT INTO alertas(prod_id) VALUES (prod);
				UPDATE inventory SET stock=stock-(select quantity from orderdetail where prod_id=prod and orderid=ord) where prod_id=prod;
				UPDATE inventory SET sales=sales+(select quantity from orderdetail where prod_id=prod and orderid=ord) where prod_id=prod;	
			-- En cualquier otro caso compra
			ELSE
				UPDATE inventory SET stock=stock-(select quantity from orderdetail where prod_id=prod and orderid=ord) where prod_id=prod;
				UPDATE inventory SET sales=sales+(select quantity from orderdetail where prod_id=prod and orderid=ord) where prod_id=prod;
			END IF;
		END LOOP;
	return NEW;
	
	END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER updInventory after update on orders
FOR EACH ROW WHEN (old.status is distinct from new.status) EXECUTE PROCEDURE triggerUpdate();