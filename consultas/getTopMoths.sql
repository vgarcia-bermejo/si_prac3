﻿CREATE OR REPLACE FUNCTION gettopmonths(produmbral integer, dineroumbral integer) RETURNS TABLE(anyos text, meses text) AS $$
	DECLARE
		produmb alias for $1;
		dineroumb alias for $2;
		
	BEGIN
	
	RETURN QUERY select t1.anos::text, t1.meses::text 
	from (select extract(year from orderdate) as anos, extract(month from orderdate) as meses, SUM(totalamount) as dinero, count(totalamount) as ventas from orders group by anos, meses order by anos desc,meses desc) as t1
	where t1.ventas>produmb OR t1.dinero>=dineroumb;
END;
$$	
LANGUAGE plpgsql;