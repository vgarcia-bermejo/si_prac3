<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Pagina de compra de Películas">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Peliculas La Foca </title>
</head>

<body>
  <!-- Primer menu -->

  <div class="menu">
    <div class="logo">
      <a href="record.php"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
    </div>
    <div class ="nombre">
      <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>
    </div>


      <?php
        session_start();
        $usuario = $_SESSION["usuario"];
        if(isset($usuario)){
          print('<div class="login">
            <a href="./historial.php">'.$usuario.'</a>
          </div>
          <div class="registro">
            <a href="./destroysession.php">Logout</a>
          </div>');
        }else{
          print('<div class="login">
            <a href="./login.php">Login</a>
          </div>
          <div class="registro">
            <a href="./register.html">Registro</a>
            </div>');
        }
        ?>

  </div>

  <!-- Segundo Menu -->

  <div class="menu2">
    <div class="carrito">
      <a href="carro.php"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
    </div>
    <div class="buscador">
      <form method="get" action="busquedaPelicula.php"><input type="text" placeholder="SEARCH" name="q" ></form>
      <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
    </div>
</div>

    <!-- Estructura -->
    <div class ="estructura">
      <div class="col-2 lateral">
        <ul class="desplegable">
          <li><a href="">Noticias</a></li>
          <li><a href="">Estrenos</a></li>
          <li><a href="">Géneros</a>
          <ul class="generos">
            <li><a href="busquedaGenero.php?val=Horror" > Horror</a><br></li>
            <li><a href="busquedaGenero.php?val=Reality-TV" > Reality-TV</a><br></li>
            <li><a href="busquedaGenero.php?val=Sport" > Sport</a><br></li>
            <li><a href="busquedaGenero.php?val=Action" > Action</a><br></li>
            <li><a href="busquedaGenero.php?val=Mystery" > Mystery</a><br></li>
            <li><a href="busquedaGenero.php?val=Thriller" > Thriller</a><br></li>
            <li><a href="busquedaGenero.php?val=Family" > Family</a><br></li>
            <li><a href="busquedaGenero.php?val=Comedy" > Comedy</a><br></li>
            <li><a href="busquedaGenero.php?val=Film-Noir" > Film-Noir</a><br></li>
            <li><a href="busquedaGenero.php?val=Adventure" > Adventure</a><br></li>
            <li><a href="busquedaGenero.php?val=Talk-Show" > Talk-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Crime" > Crime</a><br></li>
            <li><a href="busquedaGenero.php?val=History" > History</a><br></li>
            <li><a href="busquedaGenero.php?val=Documentary" > Documentary</a><br></li>
            <li><a href="busquedaGenero.php?val=Romance" > Romance</a><br></li>
            <li><a href="busquedaGenero.php?val=Drama" > Drama</a><br></li>
            <li><a href="busquedaGenero.php?val=Fantasy" > Fantasy</a><br></li>
            <li><a href="busquedaGenero.php?val=Music" > Music</a><br></li>
            <li><a href="busquedaGenero.php?val=Short" > Short</a><br></li>
            <li><a href="busquedaGenero.php?val=Western" > Western</a><br></li>
            <li><a href="busquedaGenero.php?val=Musical" > Musical</a><br></li>
            <li><a href="busquedaGenero.php?val=Sci-Fi" > Sci-Fi</a><br></li>
            <li><a href="busquedaGenero.php?val=Animation" > Animation</a><br></li>
            <li><a href="busquedaGenero.php?val=Biography" > Biography</a><br></li>
            <li><a href="busquedaGenero.php?val=War" > War</a><br></li>
            <li><a href="busquedaGenero.php?val=Game-Show" > Game-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Adult" > Adult</a><br></li>  
          </ul>
          </li>
        </ul>
      </div>
      <form action="./film.php" method ="post">
      <?php
        session_start();
        $titlee = $_GET["q"];
        $contador = 0;
        $contadorpelis = 0;

        try{
        $dbh = NEW PDO("pgsql:dbname=si1; host=localhost;", "alumnodb", "alumnodb");
        }
        catch(PDOException $e){
          echo $e->getMessage();
          header('Location: login.php');
        }

        print('<div class="col-9 contenido">');


        $sql= "select movieid,movietitle from imdb_movies where movietitle like '%".$titlee."%'";
        

        foreach ($dbh->query($sql) as $row) {
          if($contador == 0){
            print('<div class="fila">');
          }
          print('<div class="col-4" >
                <div class="car">
                  <a href="film.php?val='.$row['movietitle'].'"><img class="'.$row['movietitle'].'" src="media/generica.jpg" alt="'.$row['movietitle'].'"></a>
                  <br>
                  <p class="titulopelicula">'.$row['movietitle'].'</p>
                </div>
              </div>
              ');

              if($contador == 2){
                print("</div>");
                $contador =-1;
              }
              $contador = $contador +1;
              $contadorpelis = $contadorpelis +1;
        }
        if($contadorpelis == 0){
            print('<h2> No se encontraron peliculas con este titulo. Por favor pruebe con otro. </h2>');

        }

        if($contador != 2){
            print('</div>');
        }

        if($contadorpelis <= 2){
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
        }
        if($contadorpelis <= 5){
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
            print('<div class ="fila">
            </div>');
        }
        print('</div>');


      ?>
     </form>
    <!-- footer -->
  <div class="filafooter">
    <p class="copyright"><br><br>© Copyright by Javier Encinas y Víctor García-Bermejo. All Rights Reserved.</p>
  </div>
</body>
</html>
