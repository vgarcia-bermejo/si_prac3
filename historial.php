<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Ficha de la película">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Ficha de Película </title>
</head>

<body>
    <!-- Primer menu -->


      <div class="menu">
        <div class="logo">
          <a href="record.php"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
        </div>
        <div class ="nombre">
          <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>
        </div>


          <?php
            session_start();
            $usuario = $_SESSION["usuario"];
            if(isset($usuario)){
              print('<div class="login">
                <a href="./historial.php">'.$usuario.'</a>
              </div>
              <div class="registro">
                <a href="./destroysession.php">Logout</a>
              </div>');
            }else{
              print('<div class="login">
                <a href="./login.php">Login</a>
              </div>
              <div class="registro">
                <a href="./register.html">Registro</a>
                </div>');
            }
            ?>

      </div>

      <!-- Segundo Menu -->

      <div class="menu2">
        <div class="carrito">
          <a href="./carrito.php"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
        </div>
        <div class="buscador">
          <form method="get" action="busquedaPelicula.php"><input type="text" placeholder="SEARCH" name="q" ></form>
          <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
        </div>
    </div>

    <!-- Estructura -->
  <div class ="estructura">
    <div class="col-2 lateral">
      <ul class="desplegable">
        <li><a href="">Noticias</a></li>
        <li><a href="">Estrenos</a></li>
        <li><a href="">Géneros</a>
        <ul class="generos">
            <li><a href="busquedaGenero.php?val=Horror" > Horror</a><br></li>
            <li><a href="busquedaGenero.php?val=Reality-TV" > Reality-TV</a><br></li>
            <li><a href="busquedaGenero.php?val=Sport" > Sport</a><br></li>
            <li><a href="busquedaGenero.php?val=Action" > Action</a><br></li>
            <li><a href="busquedaGenero.php?val=Mystery" > Mystery</a><br></li>
            <li><a href="busquedaGenero.php?val=Thriller" > Thriller</a><br></li>
            <li><a href="busquedaGenero.php?val=Family" > Family</a><br></li>
            <li><a href="busquedaGenero.php?val=Comedy" > Comedy</a><br></li>
            <li><a href="busquedaGenero.php?val=Film-Noir" > Film-Noir</a><br></li>
            <li><a href="busquedaGenero.php?val=Adventure" > Adventure</a><br></li>
            <li><a href="busquedaGenero.php?val=Talk-Show" > Talk-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Crime" > Crime</a><br></li>
            <li><a href="busquedaGenero.php?val=History" > History</a><br></li>
            <li><a href="busquedaGenero.php?val=Documentary" > Documentary</a><br></li>
            <li><a href="busquedaGenero.php?val=Romance" > Romance</a><br></li>
            <li><a href="busquedaGenero.php?val=Drama" > Drama</a><br></li>
            <li><a href="busquedaGenero.php?val=Fantasy" > Fantasy</a><br></li>
            <li><a href="busquedaGenero.php?val=Music" > Music</a><br></li>
            <li><a href="busquedaGenero.php?val=Short" > Short</a><br></li>
            <li><a href="busquedaGenero.php?val=Western" > Western</a><br></li>
            <li><a href="busquedaGenero.php?val=Musical" > Musical</a><br></li>
            <li><a href="busquedaGenero.php?val=Sci-Fi" > Sci-Fi</a><br></li>
            <li><a href="busquedaGenero.php?val=Animation" > Animation</a><br></li>
            <li><a href="busquedaGenero.php?val=Biography" > Biography</a><br></li>
            <li><a href="busquedaGenero.php?val=War" > War</a><br></li>
            <li><a href="busquedaGenero.php?val=Game-Show" > Game-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Adult" > Adult</a><br></li>
        </ul>
        </li>
      </ul>
    </div>
          <?php
          session_start();

          $usuario=$_SESSION["usuario"];

          try{
          $dbh = NEW PDO("pgsql:dbname=si1; host=localhost;", "alumnodb", "alumnodb");
          }
          catch(PDOException $e){
            echo $e->getMessage();
            header('Location: login.php');
          }

          $sql1="select customerid from customers where username like '".$usuario."';";
          $stmt = $dbh->query($sql1);
          $result = $stmt->fetch();
          $id= $result["customerid"];

          $sql = "select movietitle, t4.price, t4.orderdate from
(select movieid, t3.price, t3.orderdate from
(select prod_id,price,orderdate from 
(select price,prod_id,orderid from orderdetail where orderid in (select orderid from orders where customerid =".$id.")) as t1 join
(select orderdate,orderid from orders where customerid=1)as t2 on t1.orderid=t2.orderid) as t3, products
where t3.prod_id=products.prod_id) as t4,imdb_movies where t4.movieid = imdb_movies.movieid;";?>
          <div class="tablacompracontenido">
            <div class="fila titulotabla">
              <h1>Registro de compras</h1>
            </div>
          <div class="tablacompra">
            <table class="tabla">
              <tr>
              <th>Pelicula</th>
              <th>Precio</th>
              <th>Fecha</th>
            </tr>
          <?php
          foreach ($dbh->query($sql) as $row) {
            
            echo "  <tr>";
            echo "    <td>".$row["movietitle"]."</td>";
            echo "    <td>".$row["price"]."</td>";
            echo "    <td>".$row["orderdate"]."</td>";
            echo "  </tr>";
          }

	          ?>
        </table>

      </div>


    </div>
    </div>

  <!-- footer -->
  <footer class="filafooter">
    <p class="copyright"><br><br>© Copyright by Javier Encinas y Víctor García-Bermejo. All Rights Reserved.</p>
  </footer>
</body>
</html>
