<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Pagina de compra de Películas">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Peliculas La Foca </title>
</head>

<body>
  <!-- Primer menu -->


    <div class="menu">
      <div class="logo">
        <a href="historial.php"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
      </div>
      <div class ="nombre">
        <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>
      </div>


        <?php

          session_start();
          $db = new PDO("pgsql:dbname=si1; host=localhost","alumnodb","alumnodb");
          $usuario = $_SESSION["usuario"];
          if(isset($usuario)){
            print('<div class="login">
              <a href="./historial.php">'.$usuario.'</a>
            </div>
            <div class="registro">
              <a href="./destroysession.php">Logout</a>
            </div>');
          }else{
            print('<div class="login">
              <a href="./login.php">Login</a>
            </div>
            <div class="registro">
              <a href="./register.html">Registro</a>
              </div>');
          }
          ?>

    </div>

    <!-- Segundo Menu -->

    <div class="menu2">
      <div class="carrito">
        <a href="carro.php"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
      </div>
      <div class="buscador">
        <form method="get" action="busquedaPelicula.php"><input type="text" placeholder="SEARCH" name="q" ></form>
        <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
      </div>
  </div>
    <!-- Estructura -->
  <div class ="estructura">
    <div class="col-2 lateral">
      <ul class="desplegable">
        <li><a href="">Noticias</a></li>
        <li><a href="">Estrenos</a></li>
        <li><a href="">Géneros</a>
        <ul class="generos">
            <li><a href="busquedaGenero.php?val=Horror" > Horror</a><br></li>
            <li><a href="busquedaGenero.php?val=Reality-TV" > Reality-TV</a><br></li>
            <li><a href="busquedaGenero.php?val=Sport" > Sport</a><br></li>
            <li><a href="busquedaGenero.php?val=Action" > Action</a><br></li>
            <li><a href="busquedaGenero.php?val=Mystery" > Mystery</a><br></li>
            <li><a href="busquedaGenero.php?val=Thriller" > Thriller</a><br></li>
            <li><a href="busquedaGenero.php?val=Family" > Family</a><br></li>
            <li><a href="busquedaGenero.php?val=Comedy" > Comedy</a><br></li>
            <li><a href="busquedaGenero.php?val=Film-Noir" > Film-Noir</a><br></li>
            <li><a href="busquedaGenero.php?val=Adventure" > Adventure</a><br></li>
            <li><a href="busquedaGenero.php?val=Talk-Show" > Talk-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Crime" > Crime</a><br></li>
            <li><a href="busquedaGenero.php?val=History" > History</a><br></li>
            <li><a href="busquedaGenero.php?val=Documentary" > Documentary</a><br></li>
            <li><a href="busquedaGenero.php?val=Romance" > Romance</a><br></li>
            <li><a href="busquedaGenero.php?val=Drama" > Drama</a><br></li>
            <li><a href="busquedaGenero.php?val=Fantasy" > Fantasy</a><br></li>
            <li><a href="busquedaGenero.php?val=Music" > Music</a><br></li>
            <li><a href="busquedaGenero.php?val=Short" > Short</a><br></li>
            <li><a href="busquedaGenero.php?val=Western" > Western</a><br></li>
            <li><a href="busquedaGenero.php?val=Musical" > Musical</a><br></li>
            <li><a href="busquedaGenero.php?val=Sci-Fi" > Sci-Fi</a><br></li>
            <li><a href="busquedaGenero.php?val=Animation" > Animation</a><br></li>
            <li><a href="busquedaGenero.php?val=Biography" > Biography</a><br></li>
            <li><a href="busquedaGenero.php?val=War" > War</a><br></li>
            <li><a href="busquedaGenero.php?val=Game-Show" > Game-Show</a><br></li>
            <li><a href="busquedaGenero.php?val=Adult" > Adult</a><br></li>  
        </ul>
        </li>
      </ul>
    </div>


    <?php
      session_start();

      $title = $_GET["val"];
	  if(strpos($title,"'") != FALSE){
		$numerocaracteres = strlen($titulo);
		$posicion = strpos($title,"'");
		$title = substr_replace($title, "'", $posicion, 0);
		$flag = 1;
	}

      $_SESSION["peliaux"] = $title;
      if(isset($_GET["val"])){
      	if($flag==1){
      		$sql = "Select * from imdb_movies where movietitle like '%Bug''s Life, A%'";
      	}else{
        	$sql = "Select * from imdb_movies where movietitle = '".$title."'";
    	}
        $row = $db->query($sql);
        $rowfetched = $row->fetch();
            if($title == $rowfetched["movietitle"]){
              ?>
              <form action = "./sesionaddcarrito.php" method ="post">
              <div class="col-10 contenido">
                      <div class="fila fichatitulo">
                        <br><h1><?php echo $rowfetched["movietitle"] ?></h1>
                      </div>
                      <div class="fila informacion">
                        <div class="col-9 fotoo">
                          <img class="foto" alt="Portada pelicula" src="media/generica.jpg">
                        </div>
                        <div class="col-7 datoscompletos">
                          <ul class="datoscompletoslista">
                            <li><b>Año: </b><?php echo $rowfetched["year"];?><br></li>

                            <li><br><?php
                             $sql = "Select distinct directorname from imdb_directors natural join imdb_directormovies
                                          natural join imdb_movies where movietitle like '".$title."'";
                             $query = $db->query($sql);
                             $dicfetch = $query->fetch();
                             echo "<br><b>Director: </b>";
                             echo $dicfetch["directorname"];?></li>
                
                          <li><br><?php
                          $sql = "Select distinct actorname from imdb_actors natural join imdb_actormovies
                                       natural join imdb_movies where movietitle like '".$title."'";
                          $query = $db->query($sql);

                          echo "<b>Reparto: </b>";
                          foreach ($db->query($sql) as $pelicula){
                            echo $pelicula["actorname"].', ';
                          }
                          ?></li>
                            <br>
                            <li><b>Generos: </b><?php
                            $sql = "Select distinct genero from imdb_genres natural join imdb_moviegenres
                                         natural join imdb_movies where movietitle like '".$title."'";



                            foreach ($db->query($sql) as $pelicula){
                              echo $pelicula["genero"].', ';
                            }?></li>
                          </ul>
                        </div>
                      </div>


                    <div class="fila botoncompra">
                      <input class="button" type="submit" value="Añadir al carro" name=Anyadir>
                    </div>

                  </div>
                </form>
              </div>
                <?php

        }
      }

     ?>

</body>
</html>
