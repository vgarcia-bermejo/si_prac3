﻿

alter table orderdetail add foreign key (prod_id)
	references products(prod_id) on update cascade;
alter table orderdetail add foreign key (orderid)
	references orders(orderid) on update cascade;
create table Imdb_genremovies(
	movieid serial FOREIGN KEY references imdb_movies(movieid) on update cascade,
	genre VARCHAR(32) FOREIGN KEY references imdb_moviegenres(genre) on update cascade
);
----------------------------------------------------------------------------------------------------
 create table imdb_genres(
	genero VARCHAR(32) primary key
 );

insert into imdb_genres (genero) select distinct genre from imdb_moviegenres;

--drop table auxgenre;

alter table imdb_moviegenres add foreign key (genre) references imdb_genres (genero) on update cascade;
----------------------------------------------------------------------------------------------------
 create table imdb_countries(
	pais VARCHAR(32) primary key
 );

insert into imdb_countries (pais) select distinct country from imdb_moviecountries;

--drop table auxcountry;

alter table imdb_moviecountries add foreign key (country) references imdb_countries (pais) on update cascade;

----------------------------------------------------------------------------------------------------
 create table imdb_languages(
	idioma VARCHAR(32) primary key
 );

insert into imdb_languages (idioma) select distinct language from imdb_movielanguages;

--drop table auxcountry;

alter table imdb_movielanguages add foreign key (language) references imdb_languages (idioma) on update cascade;
